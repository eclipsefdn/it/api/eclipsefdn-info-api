CREATE TABLE `Trademark` (
  `category` varchar(16) NOT NULL,
  `id` varchar(64) NOT NULL,
  `name` varchar(256) NOT NULL,
  `demarcation` varchar(10) DEFAULT NULL,
  `type` char(1) DEFAULT NULL,
  KEY `category_index` (`category`),
  KEY `id_index` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;