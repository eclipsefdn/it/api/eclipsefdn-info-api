SHELL = /bin/bash
pre-install:;
	mkdir -p ./config/mariadb/initdb.d
dev-start:;
	mvn compile -e quarkus:dev -Dconfig.secret.path=$$PWD/config/application/secret.properties
clean:;
	mvn clean
install-yarn:;
	yarn install --frozen-lockfile --audit
generate-spec: validate-spec;
	yarn run generate-json-schema
validate-spec: install-yarn;
compile-java: generate-spec;
	mvn compile package
compile-java-quick: generate-spec;
	mvn compile package -Dmaven.test.skip=true
compile-micro: generate-spec;
	mvn package -Dnative -Dquarkus.native.native-image-xmx=5G
compile-micro-local: generate-spec;
	mvn package -Dnative -Dquarkus.native.container-build=true -Dquarkus.container-image.build=true
compile: clean compile-java;
compile-quick: clean compile-java-quick;
compile-start: compile-quick;
	docker compose down
	docker compose build
	docker compose up -d	
compile-start-micro: compile-micro-local;
	docker build -f src/main/docker/Dockerfile.native-micro -t local/eclipsefdn-info-api:micro-latest .
	docker compose --profile=native down
	docker compose build
	docker compose --profile=native up -d
