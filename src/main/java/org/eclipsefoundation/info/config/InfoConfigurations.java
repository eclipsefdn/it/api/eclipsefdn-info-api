/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.info.config;

import java.util.List;
import java.util.Map;

import io.smallrye.config.ConfigMapping;

/**
 * 
 */
@ConfigMapping(prefix = "eclipse.info")
public interface InfoConfigurations {

    BoardConfigurations board();

    Map<String, CouncilConfigurations> council();
    
    public interface BoardConfigurations {
        List<String> foundationCountries();

        List<String> appointedRelations();

        List<String> electedRelations();
    }
    public interface CouncilConfigurations {
        String chairRelation();
        
        List<String> relations();
    }
}
