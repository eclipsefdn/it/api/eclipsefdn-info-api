/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.info.config;

import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullPeopleRelationData;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;

import io.quarkus.runtime.annotations.RegisterForReflection;

/**
 * Required as external client lib does not retain the reflection annotation at build. This enables the data to be properly serialized by
 * the JSON engine.
 */
@RegisterForReflection(targets = { FullOrganizationContactData.class, OrganizationData.class, PeopleData.class, SysRelationData.class,
        FullPeopleRelationData.class })
public class ExternalReflectionConfiguration {
    // intentionally empty, as the annotations drive reflection registration in native compilation.
}
