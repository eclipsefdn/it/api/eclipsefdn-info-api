/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.info.models;

import java.util.List;

import io.quarkus.runtime.annotations.RegisterForReflection;

/**
 * Contains record information about Council Member classes used for output in API responses.
 */
@RegisterForReflection
public interface CouncilMemberData {

    /**
     * Data model for CouncilMember API outputs.
     */
    record Result(String personId, String firstName, String lastName, String orgName, Integer orgId, String comment,
            List<String> mentorForProjects, boolean isChair, FullRelation relation) {
    }

    /**
     * Contains more full data about a relation than would otherwise be available
     */
    record FullRelation(String relation, String description) {
    }
}
