/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.info.dtos.filters;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.info.dtos.Trademark;
import org.eclipsefoundation.info.namespaces.InfoUrlParameterNames;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Standard DTO filter for the Trademark table
 */
@Singleton
public class TrademarkFilter implements DtoFilter<Trademark> {
    @Inject
    ParameterizedSQLStatementBuilder builder;

    @Override
    public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
        ParameterizedSQLStatement stmt = builder.build(Trademark.TABLE);
        // category check
        String category = params.getFirst(InfoUrlParameterNames.CATEGORY_VALUE);
        if (StringUtils.isNotBlank(category)) {
            stmt
                    .addClause(new ParameterizedSQLStatement.Clause(Trademark.TABLE.getAlias() + ".id.category = ?",
                            new Object[] { category }));
        }
        // type check
        List<String> tmType = params.get(InfoUrlParameterNames.TRADEMARK_TYPE_VALUE);
        if (tmType != null && !tmType.isEmpty()) {
            stmt.addClause(new ParameterizedSQLStatement.Clause(Trademark.TABLE.getAlias() + ".type in ?", new Object[] { tmType }));
        }
        return stmt;
    }

    @Override
    public Class<Trademark> getType() {
        return Trademark.class;
    }

}
