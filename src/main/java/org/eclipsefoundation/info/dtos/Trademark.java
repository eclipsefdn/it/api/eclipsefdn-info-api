/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.info.dtos;

import java.util.Objects;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.DtoTable;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

/**
 * Contains information about an Eclipse Foundation trademarked entity, either standard or registered.
 */
@Table
@Entity
public class Trademark extends BareNode {
    public static final DtoTable TABLE = new DtoTable(Trademark.class, "tm");

    @EmbeddedId
    private TrademarkId compositeId;
    private String name;
    private String demarcation;
    private String type;

    @Override
    public Object getId() {
        return getCompositeId();
    }

    /**
     * @return the compositeId
     */
    public TrademarkId getCompositeId() {
        return compositeId;
    }

    /**
     * @param compositeId the compositeId to set
     */
    public void setCompositeId(TrademarkId compositeId) {
        this.compositeId = compositeId;
    }

    /**
     * @return the demarcation
     */
    public String getDemarcation() {
        return demarcation;
    }

    /**
     * @param demarcation the demarcation to set
     */
    public void setDemarcation(String demarcation) {
        this.demarcation = demarcation;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(compositeId, demarcation, name, type);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Trademark other = (Trademark) obj;
        return Objects.equals(compositeId, other.compositeId) && Objects.equals(demarcation, other.demarcation)
                && Objects.equals(name, other.name) && Objects.equals(type, other.type);
    }

    @Embeddable
    public static final class TrademarkId {
        private String id;
        private String category;

        /**
         * @return the id
         */
        public String getId() {
            return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         * @return the category
         */
        public String getCategory() {
            return category;
        }

        /**
         * @param category the category to set
         */
        public void setCategory(String category) {
            this.category = category;
        }

        @Override
        public int hashCode() {
            return Objects.hash(category, id);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            TrademarkId other = (TrademarkId) obj;
            return Objects.equals(category, other.category) && Objects.equals(id, other.id);
        }

    }
}
