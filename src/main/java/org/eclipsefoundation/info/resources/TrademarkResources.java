/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.info.resources;

import java.util.List;
import java.util.Optional;

import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.CacheControlCommonValues;
import org.eclipsefoundation.info.dtos.Trademark;
import org.eclipsefoundation.info.models.TrademarkData;
import org.eclipsefoundation.info.namespaces.InfoUrlParameterNames;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.utils.helper.StreamHelper;
import org.jboss.resteasy.reactive.Cache;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MultivaluedHashMap;

/**
 * API endpoints for retrieving full or filtered views of trademark data for the Eclipse Foundation.
 */
@Path("trademarks")
public class TrademarkResources {

    private CachingService cache;
    private PersistenceDao dao;
    private FilterService filters;

    private RequestWrapper wrapper;

    /**
     * Default constructor for the resource.
     * 
     * @param cache custom cache service access for standardized caching
     * @param dao access object for interacting with the database
     * @param filters DTO filter service for retrieving filters for DTO requests
     * @param wrapper request scoped wrapper object for implicitly inheriting URL parameters for DTO filters.
     */
    public TrademarkResources(CachingService cache, PersistenceDao dao, FilterService filters, RequestWrapper wrapper) {
        this.cache = cache;
        this.dao = dao;
        this.filters = filters;
        this.wrapper = wrapper;
    }

    /**
     * Retrieve trademark data, with an optional trademark type filter.
     * 
     * @param trademarkType the type of trademark to filter by, such as 'r' for registered, or 't' for trademarked.
     * @return list of trademarks with the given type if set, or all if none set.
     */
    @GET
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public List<TrademarkData> getTrademarks(@QueryParam(InfoUrlParameterNames.TRADEMARK_TYPE_VALUE) List<String> trademarkTypes) {
        // param map used for lookups + cache narrowing
        MultivaluedHashMap<String, String> params = new MultivaluedHashMap<>();
        params.put(InfoUrlParameterNames.TRADEMARK_TYPE_VALUE, trademarkTypes);

        // create query and set limit to false
        RDBMSQuery<Trademark> q = new RDBMSQuery<>(wrapper, filters.get(Trademark.class), params);
        q.setUseLimit(false);

        // get the cached data
        Optional<List<TrademarkData>> out = cache.get("all", params, Trademark.class, () -> mapTrademarkData(dao.get(q))).data();
        if (out.isEmpty() || out.get().isEmpty()) {
            throw new NotFoundException("No trademarks found for current parameters");
        }
        return out.get();
    }

    /**
     * Retrieve trademark data for a given category, with an optional trademark type filter.
     * 
     * @param category the category grouping for trademarks to retrieve entries for
     * @param trademarkType the type of trademark to filter by, such as 'r' for registered, or 't' for trademarked.
     * @return list of trademarks that match the given filters if any.
     */
    @GET
    @Path("{category}")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public List<TrademarkData> getTrademarksByCategory(@PathParam("category") String category,
            @QueryParam(InfoUrlParameterNames.TRADEMARK_TYPE_VALUE) List<String> trademarkTypes) {
        // param map used for lookups + cache narrowing
        MultivaluedHashMap<String, String> params = new MultivaluedHashMap<>();
        params.put(InfoUrlParameterNames.TRADEMARK_TYPE_VALUE, trademarkTypes);
        params.add(InfoUrlParameterNames.CATEGORY_VALUE, category);

        // create query and set limit to false
        RDBMSQuery<Trademark> q = new RDBMSQuery<>(wrapper, filters.get(Trademark.class), params);
        q.setUseLimit(false);

        // get the cached data
        Optional<List<TrademarkData>> out = cache
                .get("category:" + category, params, Trademark.class, () -> mapTrademarkData(dao.get(q)))
                .data();
        if (out.isEmpty() || out.get().isEmpty()) {
            throw new NotFoundException("No trademarks found for current parameters");
        }
        return out.get();
    }

    private List<TrademarkData> mapTrademarkData(List<Trademark> trademarks) {
        return trademarks
                .stream()
                .filter(StreamHelper.distinctByKey(Trademark::getName))
                .map(t -> new TrademarkData(t.getCompositeId().getId(), t.getCompositeId().getCategory(), t.getName(), t.getDemarcation(),
                        t.getType()))
                .toList();
    }
}
