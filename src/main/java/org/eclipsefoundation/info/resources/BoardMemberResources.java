/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.info.resources;

import java.util.List;

import org.eclipsefoundation.http.namespace.CacheControlCommonValues;
import org.eclipsefoundation.info.config.InfoConfigurations;
import org.eclipsefoundation.info.models.BoardMemberData;
import org.eclipsefoundation.info.service.FoundationService;
import org.jboss.resteasy.reactive.Cache;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

/**
 * API endpoint for retrieving public board member data for the Eclipse Foundation.
 */
@Path("board-members")
public class BoardMemberResources {

    private final InfoConfigurations config;
    private final FoundationService foundation;

    /**
     * Default constructor for endpoint.
     * 
     * @param foundation instance of Foundation DB API caching service to provide access to board member relation entries.
     * @param countryCodes list of supported foundation country codes from the configuration.
     */
    public BoardMemberResources(FoundationService foundation, InfoConfigurations config) {
        this.foundation = foundation;
        this.config = config;
    }

    /**
     * Retrieves a list of board members for the given country code, so long as the country code is supported in configurations. Examples of
     * this are `us` for the Eclipse Foundation .inc board members, and `be` for members of the Eclipse Foundation AISBL board of directors.
     * 
     * @param foundationCountry the country code of the Foundation to retrieve board members for.
     * @return list of board members discovered for the given Eclipse Foundation entity
     */
    @GET
    @Path("{foundationCountry}")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public List<BoardMemberData.Result> getBoardMembers(@PathParam("foundationCountry") String foundationCountry) {
        // check that the country code in question is tracked as a supported country code
        String normalizedFoundationCountry = foundationCountry.toUpperCase();
        if (!config.board().foundationCountries().contains(normalizedFoundationCountry)) {
            throw new BadRequestException("Passed Eclipse Foundation country is invalid: " + foundationCountry);
        }
        // get the directors for the given Foundation, matching on the country code of the foundation.
        return foundation
                .getBoardDirectors()
                .stream()
                .filter(bd -> bd.relation().code().endsWith(normalizedFoundationCountry))
                .toList();
    }
}
