/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.info.resources;

import java.util.List;
import java.util.Optional;

import org.eclipsefoundation.http.namespace.CacheControlCommonValues;
import org.eclipsefoundation.info.config.InfoConfigurations;
import org.eclipsefoundation.info.models.CouncilMemberData.Result;
import org.eclipsefoundation.info.service.FoundationService;
import org.jboss.resteasy.reactive.Cache;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

/**
 * 
 */
@Path("councils")
public class CouncilResources {

    private final InfoConfigurations config;
    private final FoundationService foundation;

    /**
     * Default constructor for endpoint.
     * 
     * @param foundation instance of Foundation DB API caching service to provide access to council relation entries.
     * @param countryCodes list of supported foundation country codes from the configuration.
     */
    public CouncilResources(FoundationService foundation, InfoConfigurations config) {
        this.foundation = foundation;
        this.config = config;
    }

    @GET
    @Path("{councilName}")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public List<Result> getCouncil(@PathParam("councilName") String code) {
        Optional<String> matchingCouncil = config.council().keySet().stream().filter(s -> s.equalsIgnoreCase(code)).findFirst();
        if (matchingCouncil.isEmpty()) {
            throw new BadRequestException("Passed code is not a recognised council code");
        }
        return foundation.getCouncilMembers(matchingCouncil.get());
    }
}
