/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.info.service;

import java.util.List;

import org.eclipsefoundation.info.models.BoardMemberData;
import org.eclipsefoundation.info.models.CouncilMemberData;

/**
 * Service made to interact with and cache upstream Foundation DB data.
 */
public interface FoundationService {

    /**
     * Retrieves a list of board of director members, using the common relations to fetch the associated contact and people relation
     * entries.
     * 
     * @return list of all board of director members to be filtered downstream as needed.
     */
    List<BoardMemberData.Result> getBoardDirectors();
    
    /**
     * Retrieves a list of council members for the given council code.
     * 
     * @return list of all council members for the given council code.
     */
    List<CouncilMemberData.Result> getCouncilMembers(String councilCode);
}
