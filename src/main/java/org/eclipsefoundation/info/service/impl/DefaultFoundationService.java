/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.info.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullPeopleProjectData;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullPeopleRelationData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;
import org.eclipsefoundation.info.apis.FoundationAPI;
import org.eclipsefoundation.info.config.InfoConfigurations;
import org.eclipsefoundation.info.config.InfoConfigurations.CouncilConfigurations;
import org.eclipsefoundation.info.models.BoardMemberData;
import org.eclipsefoundation.info.models.BoardMemberData.FullRelation;
import org.eclipsefoundation.info.models.CouncilMemberData;
import org.eclipsefoundation.info.service.FoundationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.logging.Log;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.ServerErrorException;
import jakarta.ws.rs.WebApplicationException;

/**
 * Default implementation of Foundation Service, uses REST API to connect and retrieve foundation information to be served through this API.
 * Used to interact with the foundation DB in a central and cached manor.
 */
@ApplicationScoped
public class DefaultFoundationService implements FoundationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultFoundationService.class);

    private final CachingService cache;
    private final APIMiddleware middle;
    private final FoundationAPI api;
    private final InfoConfigurations config;

    /**
     * Default constructor for the Foundation service.
     * 
     * @param cache custom caching service for storing processed board member results.
     * @param middle API service used to auto paginate data
     * @param api REST client for the Foundation DB API, used to access foundation data.
     */
    public DefaultFoundationService(CachingService cache, APIMiddleware middle, @RestClient FoundationAPI api, InfoConfigurations config) {
        this.cache = cache;
        this.middle = middle;
        this.api = api;
        this.config = config;
    }

    @Override
    public List<BoardMemberData.Result> getBoardDirectors() {
        return cache.get("all", null, BoardMemberData.Result.class, () -> {
            // get the appointed reps from company records
            List<BoardMemberData.Result> out = new ArrayList<>(middle
                    .getAll(i -> api.getOrganizationContactsByRelations(i, config.board().appointedRelations(), null, true))
                    .stream()
                    .map(oc -> new BoardMemberData.Result(oc.person().fname(), oc.person().lname(), oc.organization().name1(),
                            oc.organization().organizationID(),
                            new FullRelation(oc.sysRelation().relation(), oc.sysRelation().description())))
                    .toList());
            // retrieve the elected committer board reps
            out
                    .addAll(middle
                            .getAll(i -> api.getPeopleRelationsByRelations(i, config.board().electedRelations(), true))
                            .stream()
                            .map(pr -> new BoardMemberData.Result(pr.person().fname(), pr.person().lname(), null, null,
                                    new FullRelation(pr.relation().relation(), pr.relation().description())))
                            .toList());

            return out;
        }).data().orElseThrow(() -> new ServerErrorException("Unable to load board of directors at this time", 500));
    }

    @Override
    public List<CouncilMemberData.Result> getCouncilMembers(String councilCode) {
        return cache.get(councilCode, null, CouncilMemberData.Result.class, () -> {
            // retrieve the configs for the given council
            CouncilConfigurations council = config.council().get(councilCode);
            if (council == null) {
                throw new BadRequestException();
            }

            // get council chairs, defaulting to an empty list
            List<FullPeopleProjectData> chairs = Collections.emptyList();
            try {
                chairs = middle.getAll(i -> api.getPeopleProjectsByRelations(i, Arrays.asList(council.chairRelation()), null, true));
            } catch (WebApplicationException e) {
                LOGGER.warn("Unable to fetch chairs, results may be inaccurate for this cache cycle", e);
            }

            // get the appointed reps from various records
            List<CouncilMemberData.Result> out = new ArrayList<>();
            out.addAll(getAppointedCouncilMembers(council.relations(), chairs));
            out.addAll(getCompanyCouncilMembers(council.relations(), chairs));
            out.addAll(getProjectCouncilMembers(council.relations(), chairs));
            return out;
        }).data().orElseThrow(() -> new ServerErrorException("Unable to load board of directors at this time", 500));
    }

    /**
     * Retrieve appointed council members by fetching people relation records with a council relation code. Additionally fetch associated
     * organization contact records where available to map company appointed users.
     * 
     * @param relationCodes the list of relation codes for the current council
     * @param chairs the prefetched list of chairs for the current council
     * @return list of council members that are appointed
     */
    private List<CouncilMemberData.Result> getAppointedCouncilMembers(List<String> relationCodes, List<FullPeopleProjectData> chairs) {
        return generateRecord(middle.getAll(i -> api.getPeopleRelationsByRelations(i, relationCodes, true)), chairs, pr -> pr.person(),
                pr -> pr.relation(), this::generateAppointedComment);
    }

    /**
     * Retrieve council members by fetching people project records with council relation codes. Additionally fetch associated organization
     * contact records where available to map company appointed users.
     * 
     * @param relationCodes the list of relation codes for the current council
     * @param chairs the prefetched list of chairs for the current council
     * @return list of council members that are included by project relationship
     */
    private List<CouncilMemberData.Result> getProjectCouncilMembers(List<String> relationCodes, List<FullPeopleProjectData> chairs) {
        return generateRecord(middle.getAll(i -> api.getPeopleProjectsByRelations(i, relationCodes, null, true)), chairs, pr -> pr.person(),
                pr -> pr.relation(), fppd -> fppd.project().name() + " - PMC");
    }

    /**
     * Retrieve appointed council members by fetching company relation records with given council relation codes. These members are granted
     * a seat as part of their strategic membership.
     * 
     * @param relationCodes the list of relation codes for the current council
     * @param chairs the prefetched list of chairs for the current council
     * @return list of council members that are given seats for strategic members
     */
    private List<CouncilMemberData.Result> getCompanyCouncilMembers(List<String> relationCodes, List<FullPeopleProjectData> chairs) {
        try {
            // retrieve the company council members by relation code
            List<FullOrganizationContactData> companyMembers = middle
                    .getAll(i -> api.getOrganizationContactsByRelations(i, relationCodes, null, true));
            // get mentoring relations where available
            List<FullPeopleProjectData> mentors = middle
                    .getAll(i -> api
                            .getPeopleProjectsByRelations(i, Arrays.asList("ME"),
                                    companyMembers.stream().map(p -> p.person().personID()).toList(), true));
            // convert the organization contacts to council members, checking what projects an individual is mentor for as well
            return companyMembers
                    .stream()
                    .map(p -> new CouncilMemberData.Result(p.person().personID(), p.person().fname(), p.person().lname(),
                            p.organization().name1(), p.organization().organizationID(), "Strategic Developer",
                            mentors
                                    .stream()
                                    .filter(pp -> pp.person().personID().equals(p.person().personID()))
                                    .map(pp -> pp.project().projectID())
                                    .toList(),
                            chairs.stream().anyMatch(c -> c.person().personID().equalsIgnoreCase(p.person().personID())),
                            new CouncilMemberData.FullRelation(p.sysRelation().relation(), p.sysRelation().description())))
                    .toList();
        } catch (WebApplicationException e) {
            // can happen while fetching Foundation data
            Log.error("Error while fetching company council members", e);
        }
        return Collections.emptyList();
    }

    /**
     * Genericized council member record generation logic to reduce duplication of code to convert various formats.
     * 
     * @param <T> type of record being converted to a council member record
     * @param baseRecords list of records to convert
     * @param getPersonRecord supplier that retrieves a person record for or from the base record being processed
     * @param getSysRelationData supplier that retrieves the relation record for or from the base record being processed
     * @param getComment call to generate the comment associated with the council member for the current call
     * @return list of converted council member records for the passed base records
     */
    private <T> List<CouncilMemberData.Result> generateRecord(List<T> baseRecords, List<FullPeopleProjectData> chairs,
            Function<T, PeopleData> getPersonRecord, Function<T, SysRelationData> getSysRelationData, Function<T, String> getComment) {
        try {
            // get org contact entries for council members where available
            List<String> peopleIds = baseRecords.stream().map(getPersonRecord).map(p -> p.personID()).toList();
            List<FullOrganizationContactData> companyMembers = middle
                    .getAll(i -> api.getOrganizationContactsByRelations(i, null, peopleIds, true));
            // get mentoring relations where available
            List<FullPeopleProjectData> mentors = middle
                    .getAll(i -> api.getPeopleProjectsByRelations(i, Arrays.asList("ME"), peopleIds, true));

            // using the prefetched people and available contact records, convert records into council member list and return
            return baseRecords.stream().map(br -> {
                // get the person and relation entry for the council member from the base record
                PeopleData p = getPersonRecord.apply(br);
                SysRelationData srd = getSysRelationData.apply(br);
                // if the company contact record exists, fetch it
                Optional<FullOrganizationContactData> contactRecord = companyMembers
                        .stream()
                        .filter(oc -> oc.person().personID().equals(p.personID()))
                        .findFirst();
                List<String> mentorRecords = mentors
                        .stream()
                        .filter(pp -> pp.person().personID().equals(p.personID()))
                        .map(pp -> pp.project().projectID())
                        .toList();
                // convert the people record and optional company record to the council member format
                return new CouncilMemberData.Result(p.personID(), p.fname(), p.lname(),
                        contactRecord.isPresent() ? contactRecord.get().organization().name1() : null,
                        contactRecord.isPresent() ? contactRecord.get().organization().organizationID() : null, getComment.apply(br),
                        mentorRecords, chairs.stream().anyMatch(c -> c.person().personID().equalsIgnoreCase(p.personID())),
                        new CouncilMemberData.FullRelation(srd.relation(), srd.description()));
            }).toList();
        } catch (WebApplicationException e) {
            Log.error("Error while fetching council members", e);
        }
        return Collections.emptyList();
    }

    /**
     * From the people relation record, generate the comment for the council member record that is generated
     * 
     * @param pr the people relation record for the council member in question.
     * @return comment associated with the appointed council member
     */
    private String generateAppointedComment(FullPeopleRelationData pr) {
        // get the year of the entry date (date getYear is deprecated/unsupported)
        Calendar c = Calendar.getInstance();
        c.setTime(pr.entryDate());
        return String.format("appointed (%d)", c.get(Calendar.YEAR));
    }
}
