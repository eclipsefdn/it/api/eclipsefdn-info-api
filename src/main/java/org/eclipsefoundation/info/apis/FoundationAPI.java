/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.info.apis;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullPeopleProjectData;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullPeopleRelationData;
import org.eclipsefoundation.foundationdb.client.runtime.namespaces.FoundationDBExtensionParameterNames;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;

/**
 * REST client with OIDC auth filter for interacting with the Foundation DB API.
 */
@OidcClientFilter
@RegisterRestClient(configKey = "fdndb-api")
public interface FoundationAPI {

    /**
     * Retrieve full people relation records by relation type.
     * 
     * @param params pagination base paramaters for request
     * @param relations the relations to use as filter for records
     * @return response containing the matching page of records.
     */
    @GET
    @Path("people/relations/full")
    RestResponse<List<FullPeopleRelationData>> getPeopleRelationsByRelations(@BeanParam BaseAPIParameters params,
            @QueryParam(FoundationDBExtensionParameterNames.RELATIONS_RAW) List<String> relations,
            @QueryParam(FoundationDBExtensionParameterNames.IS_ACTIVE_RAW) boolean isActive);

    /**
     * Retrieve full organization contact records, filtering by relations.
     * 
     * @param params pagination base paramaters for request
     * @param relations the relations to use as filter for records
     * @return response containing the matching page of records.
     */
    @GET
    @Path("organizations/contacts/full")
    RestResponse<List<FullOrganizationContactData>> getOrganizationContactsByRelations(@BeanParam BaseAPIParameters params,
            @QueryParam(FoundationDBExtensionParameterNames.RELATIONS_RAW) List<String> relations,
            @QueryParam(FoundationDBExtensionParameterNames.PEOPLE_IDS_RAW) List<String> peopleIds,
            @QueryParam(FoundationDBExtensionParameterNames.IS_ACTIVE_RAW) boolean isActive);

    @GET
    @Path("people/projects/full")
    RestResponse<List<FullPeopleProjectData>> getPeopleProjectsByRelations(@BeanParam BaseAPIParameters params,
            @QueryParam(FoundationDBExtensionParameterNames.RELATIONS_RAW) List<String> relations,
            @QueryParam(FoundationDBExtensionParameterNames.PEOPLE_IDS_RAW) List<String> peopleIds,
            @QueryParam(FoundationDBExtensionParameterNames.IS_ACTIVE_RAW) boolean isActive);
}
