/*
 * Copyright (C) 2024 Eclipse Foundation.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.info.namespaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipsefoundation.utils.namespace.UrlParameterNamespace;

import jakarta.inject.Singleton;

/**
 * Helper class used to provide parameter names througout the application.
 */
@Singleton
public final class InfoUrlParameterNames implements UrlParameterNamespace {
    public static final String CATEGORY_VALUE = "category";
    public static final String TRADEMARK_TYPE_VALUE = "type";
    public static final String RELATION_VALUE = "category";

    public static final UrlParameter CATEGORY = new UrlParameter(CATEGORY_VALUE);
    public static final UrlParameter TRADEMARK_TYPE = new UrlParameter(TRADEMARK_TYPE_VALUE);
    public static final UrlParameter RELATION = new UrlParameter(RELATION_VALUE);

    private static final List<UrlParameter> params = Collections.unmodifiableList(Arrays.asList(CATEGORY, TRADEMARK_TYPE, RELATION));

    @Override
    public List<UrlParameter> getParameters() {
        return new ArrayList<>(params);
    }
}