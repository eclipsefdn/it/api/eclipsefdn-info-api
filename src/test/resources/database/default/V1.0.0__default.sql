CREATE TABLE Trademark (
  category varchar NOT NULL,
  id varchar NOT NULL,
  name varchar NOT NULL,
  demarcation varchar DEFAULT NULL,
  type varchar DEFAULT NULL
);

INSERT INTO Trademark(id, category, name, demarcation, type) VALUES ('sample','project','Sample Trademark','&amp;tm;','T');
INSERT INTO Trademark(id, category, name, demarcation, type) VALUES ('registered','project','Sample Trademark','&amp;r;','R');
INSERT INTO Trademark(id, category, name, demarcation, type) VALUES ('sample','other','Other Trademark','&amp;r;','R');