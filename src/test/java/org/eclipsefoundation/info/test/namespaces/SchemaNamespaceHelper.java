/*
 * Copyright (C) 2022 Eclipse Foundation.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *          Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.info.test.namespaces;

public final class SchemaNamespaceHelper {
    public static final String BASE_SCHEMAS_PATH = "schemas/";
    public static final String BASE_SCHEMAS_PATH_SUFFIX = "-schema.json";
    public static final String TRADEMARKS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "trademarks" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String BOARD_MEMBERS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "board-members" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String COUNCIL_MEMBERS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "council-members" + BASE_SCHEMAS_PATH_SUFFIX;
}
