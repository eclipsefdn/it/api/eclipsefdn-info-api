/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.info.test.api;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullPeopleProjectData;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullPeopleRelationData;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.project.ProjectData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;
import org.eclipsefoundation.info.apis.FoundationAPI;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;

/**
 * Set up mock API to provide some hard coded samples of board member data for testing.
 */
@Mock
@RestClient
@ApplicationScoped
public class MockFoundationAPI implements FoundationAPI {

    private List<FullPeopleRelationData> peopleRelations;
    private List<FullOrganizationContactData> contacts;
    private List<FullPeopleProjectData> peopleProjects;

    public MockFoundationAPI() {
        SysRelationData srd1 = new SysRelationData("BRBE", "company rep - belgium", true, "", "");
        SysRelationData srd2 = new SysRelationData("BRUS", "company rep - us", true, "", "");
        SysRelationData srd3 = new SysRelationData("CMBE", "committer rep - belgium", true, "", "");
        SysRelationData srd4 = new SysRelationData("CMUS", "committer rep - us", true, "", "");
        SysRelationData srd5 = new SysRelationData("CMUS", "committer rep - us", true, "", "");

        PeopleData p1 = generatePerson("tester");
        PeopleData p2 = generatePerson("tester1");
        PeopleData p3 = generatePerson("tester2");
        PeopleData p4 = generatePerson("tester3");
        PeopleData p5 = generatePerson("tester4");

        OrganizationData o1 = generateOrganization(1, "sample 1");
        OrganizationData o2 = generateOrganization(2, "sample 2");
        OrganizationData o3 = generateOrganization(3, "sample 3");

        ProjectData project1 = generateProject("sample.test");
        this.peopleRelations = Arrays
                .asList(new FullPeopleRelationData(p1, srd1, new Date()), new FullPeopleRelationData(p2, srd2, new Date()),
                        new FullPeopleRelationData(p3, srd3, new Date()), new FullPeopleRelationData(p4, srd4, new Date()));
        this.contacts = Arrays
                .asList(new FullOrganizationContactData(o1, p1, srd1, "", "", ""),
                        new FullOrganizationContactData(o2, p2, srd1, "", "", ""),
                        new FullOrganizationContactData(o3, p5, srd1, "", "", ""));
        this.peopleProjects = Arrays.asList(new FullPeopleProjectData(project1, p1, srd5, new Date(), null, false));
    }

    /**
     * @return
     */
    private ProjectData generateProject(String projectId) {
        return new ProjectData(projectId, "sample", 0, "", "", "", "", null, 0, true, null, null, 0.0f, true, true);
    }

    @Override
    public RestResponse<List<FullPeopleRelationData>> getPeopleRelationsByRelations(BaseAPIParameters params, List<String> relations,
            boolean isActive) {
        return RestResponse.ok(peopleRelations.stream().filter(r -> relations.contains(r.relation().relation())).toList());
    }

    @Override
    public RestResponse<List<FullOrganizationContactData>> getOrganizationContactsByRelations(BaseAPIParameters params,
            List<String> relations, List<String> peopleIds, boolean isActive) {
        return RestResponse
                .ok(contacts
                        .stream()
                        .filter(c -> relations == null || relations.contains(c.sysRelation().relation()))
                        .filter(c -> peopleIds == null || peopleIds.contains(c.person().personID()))
                        .toList());
    }

    @Override
    public RestResponse<List<FullPeopleProjectData>> getPeopleProjectsByRelations(BaseAPIParameters params, List<String> relations,
            List<String> peopleIds, boolean isActive) {
        return RestResponse
                .ok(peopleProjects
                        .stream()
                        .filter(c -> relations == null || relations.contains(c.relation().relation()))
                        .filter(c -> peopleIds == null || peopleIds.contains(c.person().personID()))
                        .toList());
    }

    private PeopleData generatePerson(String name) {
        return new PeopleData(name, "First name", "Last name", "", false, "", "", "", "", "", "", false, false, "", new Date(), "", "", "",
                "");
    }

    private OrganizationData generateOrganization(int id, String name) {
        return new OrganizationData(id, name, "", "", "", "", new Date(), "", "", new Date());
    }
}
