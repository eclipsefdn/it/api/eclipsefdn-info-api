/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.info.resources;

import org.eclipsefoundation.info.test.namespaces.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class TrademarkResourcesTest {

    public static final String TRADEMARKS_BASE_URL = "/trademarks";
    public static final String TRADEMARKS_TYPE_FILTER_URL = TRADEMARKS_BASE_URL + "?type={type}";
    public static final String TRADEMARKS_BY_CATEGORY_URL = TRADEMARKS_BASE_URL + "/{category}";
    public static final String TRADEMARKS_BY_CATEGORY_TYPE_FILTER_URL = TRADEMARKS_BY_CATEGORY_URL + "?type={type}";
    /*
     * GET ALL TRADEMARKS
     */
    public static final EndpointTestCase ALL_TRADEMARKS_SUCCESS = TestCaseHelper
            .buildSuccessCase(TRADEMARKS_BASE_URL, new String[] {}, SchemaNamespaceHelper.TRADEMARKS_SCHEMA_PATH);
    public static final EndpointTestCase TRADEMARKS_TYPE_FILTER_SUCCESS = TestCaseHelper
            .buildSuccessCase(TRADEMARKS_TYPE_FILTER_URL, new String[] { "R" }, SchemaNamespaceHelper.TRADEMARKS_SCHEMA_PATH);
    public static final EndpointTestCase TRADEMARKS_TYPE_FILTER_NOT_FOUND = TestCaseHelper
            .buildNotFoundCase(TRADEMARKS_TYPE_FILTER_URL, new String[] { "o" }, SchemaNamespaceHelper.TRADEMARKS_SCHEMA_PATH);

    /*
     * TRADEMARKS BY CATEGORY
     */
    public static final EndpointTestCase TRADEMARKS_BY_CATEGORY_SUCCESS = TestCaseHelper
            .buildSuccessCase(TRADEMARKS_BY_CATEGORY_URL, new String[] { "project" }, SchemaNamespaceHelper.TRADEMARKS_SCHEMA_PATH);
    public static final EndpointTestCase TRADEMARKS_BY_CATEGORY_TYPE_FILTER_SUCCESS = TestCaseHelper
            .buildSuccessCase(TRADEMARKS_BY_CATEGORY_TYPE_FILTER_URL, new String[] { "project", "r" },
                    SchemaNamespaceHelper.TRADEMARKS_SCHEMA_PATH);
    public static final EndpointTestCase TRADEMARKS_BY_CATEGORY_NOT_FOUND = TestCaseHelper
            .buildNotFoundCase(TRADEMARKS_BY_CATEGORY_URL, new String[] { "meep" }, SchemaNamespaceHelper.TRADEMARKS_SCHEMA_PATH);
    public static final EndpointTestCase TRADEMARKS_BY_CATEGORY_TYPE_FILTER_NOT_FOUND = TestCaseHelper
            .buildNotFoundCase(TRADEMARKS_BY_CATEGORY_TYPE_FILTER_URL, new String[] { "project", "o" },
                    SchemaNamespaceHelper.TRADEMARKS_SCHEMA_PATH);

    /*
     * GET ALL TRADEMARKS
     */
    @Test
    void getTrademarks_success() {
        EndpointTestBuilder.from(ALL_TRADEMARKS_SUCCESS).run();
    }

    @Test
    void getTrademarks_success_typeFilter() {
        EndpointTestBuilder.from(TRADEMARKS_TYPE_FILTER_SUCCESS).run();
    }

    @Test
    void getTrademarks_success_validSchema() {
        EndpointTestBuilder.from(ALL_TRADEMARKS_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getTrademarks_success_validResponseFormat() {
        EndpointTestBuilder.from(ALL_TRADEMARKS_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getTrademarks_failure_typeNotFound() {
        EndpointTestBuilder.from(TRADEMARKS_TYPE_FILTER_NOT_FOUND).run();
    }

    /*
     * GET ALL TRADEMARKS BY CATEGORY
     */
    @Test
    void getTrademarksByCategory_success() {
        EndpointTestBuilder.from(TRADEMARKS_BY_CATEGORY_SUCCESS).run();
    }

    @Test
    void getTrademarksByCategory_success_validSchema() {
        EndpointTestBuilder.from(TRADEMARKS_BY_CATEGORY_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getTrademarksByCategory_success_validResponseFormat() {
        EndpointTestBuilder.from(TRADEMARKS_BY_CATEGORY_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getTrademarksByCategory_failure_notFound() {
        EndpointTestBuilder.from(TRADEMARKS_BY_CATEGORY_NOT_FOUND).run();
    }

    @Test
    void getTrademarksByCategory_failure_typeNotFound() {
        EndpointTestBuilder.from(TRADEMARKS_BY_CATEGORY_TYPE_FILTER_NOT_FOUND).run();
    }

}
