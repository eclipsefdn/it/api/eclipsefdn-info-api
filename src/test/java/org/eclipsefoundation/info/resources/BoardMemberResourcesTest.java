/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.info.resources;

import org.eclipsefoundation.info.test.namespaces.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class BoardMemberResourcesTest {

    public static final String BOARD_MEMBERS_BASE_URL = "/board-members/{foundation-country}";
    /*
     * GET BOARD MEMBERS BY FOUNDATION
     */
    public static final EndpointTestCase BOARD_MEMBERS_SUCCESS = TestCaseHelper
            .buildSuccessCase(BOARD_MEMBERS_BASE_URL, new String[] { "be" }, SchemaNamespaceHelper.BOARD_MEMBERS_SCHEMA_PATH);
    public static final EndpointTestCase BOARD_MEMBERS_CASE_INSENSITIVE_SUCCESS = TestCaseHelper
            .buildSuccessCase(BOARD_MEMBERS_BASE_URL, new String[] { "BE" }, SchemaNamespaceHelper.BOARD_MEMBERS_SCHEMA_PATH);
    public static final EndpointTestCase BOARD_MEMBERS_UNKNOWN_FOUNDATION = TestCaseHelper
            .buildBadRequestCase(BOARD_MEMBERS_BASE_URL, new String[] { "unknown" }, null);

    @Test
    void getBoardMembers_success() {
        EndpointTestBuilder.from(BOARD_MEMBERS_SUCCESS).run();
    }

    @Test
    void getBoardMembers_success_caseInsensitive() {
        EndpointTestBuilder.from(BOARD_MEMBERS_CASE_INSENSITIVE_SUCCESS).run();
    }

    @Test
    void getBoardMembers_success_validSchema() throws JsonMappingException, JsonProcessingException {
        EndpointTestBuilder.from(BOARD_MEMBERS_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getBoardMembers_success_validResponseFormat() {
        EndpointTestBuilder.from(BOARD_MEMBERS_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getBoardMembers_failure_foundationNotFound() {
        EndpointTestBuilder.from(BOARD_MEMBERS_UNKNOWN_FOUNDATION).run();
    }
}
