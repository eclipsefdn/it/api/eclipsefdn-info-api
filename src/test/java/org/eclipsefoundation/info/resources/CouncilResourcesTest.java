/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.info.resources;

import org.eclipsefoundation.info.test.namespaces.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class CouncilResourcesTest {

    public static final String COUNCIL_MEMBERS_BASE_URL = "/councils/{council}";
    /*
     * GET BOARD MEMBERS BY FOUNDATION
     */
    public static final EndpointTestCase COUNCIL_MEMBERS_SUCCESS = TestCaseHelper
            .buildSuccessCase(COUNCIL_MEMBERS_BASE_URL, new String[] { "architecture" }, SchemaNamespaceHelper.COUNCIL_MEMBERS_SCHEMA_PATH);
    public static final EndpointTestCase COUNCIL_MEMBERS_CASE_INSENSITIVE_SUCCESS = TestCaseHelper
            .buildSuccessCase(COUNCIL_MEMBERS_BASE_URL, new String[] { "ARCHITECTURE" }, SchemaNamespaceHelper.COUNCIL_MEMBERS_SCHEMA_PATH);
    public static final EndpointTestCase COUNCIL_MEMBERS_UNKNOWN_COUNCIL = TestCaseHelper
            .buildBadRequestCase(COUNCIL_MEMBERS_BASE_URL, new String[] { "bingus" }, SchemaNamespaceHelper.COUNCIL_MEMBERS_SCHEMA_PATH);

    @Test
    void getCouncil_success() {
        EndpointTestBuilder.from(COUNCIL_MEMBERS_SUCCESS).run();
    }

    @Test
    void getCouncil_success_caseInsensitive() {
        EndpointTestBuilder.from(COUNCIL_MEMBERS_CASE_INSENSITIVE_SUCCESS).run();
    }

    @Test
    void getCouncil_success_validSchema() {
        EndpointTestBuilder.from(COUNCIL_MEMBERS_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getCouncil_success_validResponseFormat() {
        EndpointTestBuilder.from(COUNCIL_MEMBERS_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getCouncil_failure_councilNotFound() {
        EndpointTestBuilder.from(COUNCIL_MEMBERS_UNKNOWN_COUNCIL).run();
    }
}
